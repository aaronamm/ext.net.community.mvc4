﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ext.Net.Example.MVC.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }

        public Ext.Net.MVC.PartialViewResult  EmployeeGrid(string containerId)
        {
            return new Net.MVC.PartialViewResult(containerId);

        }

    }
}
